﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core.Sorts;

namespace Project.ConsoleUI
{
    class MainMenu : BaseMenu
    {
        protected override List<MenuItem> Items => new List<MenuItem>
        {
            new MenuItem() { Text = "Сортировка методом Шелла",     Logic = new ShellSort(), IsSelected = true },
            new MenuItem() { Text = "Быстрая сортировка Хоара",     Logic = new QuickSort() },
            new MenuItem() { Text = "Сортировать записи таблицы",   Logic = new TableLogic() },
            new MenuItem() { Text = "Сортировать слова из текста в лексикографическом порядке (сложность O(N^2))", Logic = new TextSort() },
            new MenuItem() { Text = "Сортировать слова из текста в лексикографическом порядке (сложность O(nlogn))", Logic = new TextQuickSort() },
            new MenuItem() { Text = "Выход", Logic = null }
        };
    }
}
