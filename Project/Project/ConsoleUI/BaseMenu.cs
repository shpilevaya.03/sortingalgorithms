﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core;

namespace Project.ConsoleUI
{
    public abstract class BaseMenu
    {
        protected class MenuItem
        {
            public string Text;
            public bool IsSelected;
            public ILogic Logic;
        }

        protected abstract List<MenuItem> Items { get; }
        List<MenuItem> _items = new List<MenuItem>();
        FileReader fileReader = new FileReader();
        FileWriter fileWriter = new FileWriter(DataPath.output);

        public BaseMenu()
        {
            _items = Items.ToList();
        }

        public virtual void Draw()
        {
            ConsoleHelper.ClearScreen();

            foreach (var menuItem in _items)
            {
                Console.BackgroundColor = menuItem.IsSelected
                    ? Color.HighlightColor
                    : Color.BaseColor;

                Console.WriteLine("| " + menuItem.Text);
            }

            Console.BackgroundColor = Color.BaseColor;
        }

        public void Next()
        {
            var selectedItem = _items.First(x => x.IsSelected);
            var selectedInd = _items.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectedInd = selectedInd == _items.Count - 1
                ? 0
                : ++selectedInd;

            _items[selectedInd].IsSelected = true;
        }

        public void Prev()
        {
            var selectedItem = _items.First(x => x.IsSelected);

            var selectedInd = _items.IndexOf(selectedItem);

            selectedItem.IsSelected = false;

            selectedInd = selectedInd <= 0
                ? _items.Count - 1
                : --selectedInd;

            _items[selectedInd].IsSelected = true;
        }

        public bool Select()
        {
            var selectedItem = _items.First(x => x.IsSelected);

            if (selectedItem.Logic != null)
            {
                ConsoleHelper.ClearScreen();

                fileReader.ChangePath((DataPath)selectedItem.Logic.AlgorithmPath);

                selectedItem.Logic.Execute(fileReader, fileWriter);

                selectedItem.Logic.Dispose();

                Console.WriteLine();

                if (selectedItem.Logic is not TableLogic)
                {
                    ConsoleHelper.PrintHighlightedText("Для выхода нажмите любую клавишу");
                    Console.ReadKey();
                }

                Console.Clear();
            }

            return selectedItem.Logic == null;
        }

        public void SubMenu()
        {
            bool back = false;

            do
            {
                Draw();

                ConsoleKeyInfo key = Console.ReadKey(true);

                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        Next();
                        break;
                    case ConsoleKey.UpArrow:
                        Prev();
                        break;
                    case ConsoleKey.Enter:
                        var selectedItem = _items.First(x => x.IsSelected);

                        back = selectedItem.Logic == null;

                        if (!back)
                        {
                            ConsoleHelper.ClearScreen(); 

                            FileReader fileReader = new FileReader(selectedItem.Logic.AlgorithmPath);
                            FileWriter fileWriter = new FileWriter(DataPath.output);

                            selectedItem.Logic.Execute(fileReader, fileWriter);

                            selectedItem.Logic.Dispose();

                            Console.WriteLine();
                            ConsoleHelper.PrintHighlightedText("Для выхода нажмите любую клавишу");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                }
            }
            while (!back);
        }

    }
}
