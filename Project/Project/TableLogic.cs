﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.ConsoleUI;
using Project.Core;
using Project.Core.Sorts;

namespace Project
{
    class SubMenu : BaseMenu
    {
        protected override List<MenuItem> Items => new List<MenuItem>
        {
            new MenuItem() { Text = "Сортировать записи прямым слиянием",       Logic = new DirectMergeSort(), IsSelected = true },
            new MenuItem() { Text = "Сортировать записи естественным слиянием", Logic = new NaturalMergeSort() },
            new MenuItem() { Text = "Сортировать записи многопутевым слиянием", Logic = new MultipathMergeSort() },
            new MenuItem() { Text = "Выход", Logic = null }
        };
    }

    public class TableLogic : ILogic
    {
        public DataPath? AlgorithmPath => DataPath.table;

        public void Execute(FileReader fileReader, FileWriter fileWriter)
        {
            var submenu = new SubMenu();
            submenu.SubMenu();
        }

        public void Dispose() { }
    }
}
