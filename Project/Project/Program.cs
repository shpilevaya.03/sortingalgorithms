﻿using System;
using Project.Core;
using Project.Core.Sorts;
using System.Numerics;
using Project.ConsoleUI;
using Project.Core;

namespace Project
{
    class Program
    {
        public static void Main()
        {
            //var fileWriter = new FileWriter();
            //fileWriter.WriteFile(String.Join(' ', GenerationData.GenerateData(100)));

            Console.CursorVisible = false;
            MainMenu menu = new MainMenu();
            bool exit = false;

            do
            {
                menu.Draw();

                ConsoleKeyInfo key = Console.ReadKey(true);

                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        menu.Next();
                        break;
                    case ConsoleKey.UpArrow:
                        menu.Prev();
                        break;
                    case ConsoleKey.Enter:
                        exit = menu.Select();
                        break;
                }
            }
            while (!exit);
        }
    }
}