﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core
{
    public struct Color
    {
        public const ConsoleColor HighlightColor    = ConsoleColor.DarkCyan;
        public const ConsoleColor BaseColor         = ConsoleColor.Black;
    }

    static class ConsoleHelper
    {
        public static void PrintHighlightedText(string context)
        {
            Console.WriteLine(context, Console.BackgroundColor = Color.HighlightColor);
            Console.BackgroundColor = Color.BaseColor;
        }

        public static void InputNumberWithText<T>(string context, Func<T, bool> predicate, out T number) where T : IConvertible
        {
            PrintHighlightedText(context);
            number = default(T);
            while (!Is(Console.ReadLine(), ref number) || !predicate(number))
                PrintHighlightedText("Число введено неверно, повторите попытку: ");
            Console.WriteLine();
        }

        public static ConsoleColor GetLevelColor(Level level)
        {
            ConsoleColor color = ConsoleColor.Gray;
            switch (level)
            {
                case Level.Swap:    color = ConsoleColor.Magenta;   break;
                case Level.Compare: color = ConsoleColor.Cyan;      break;
                case Level.Array:   color = ConsoleColor.Blue;      break;
                case Level.Record:  color = ConsoleColor.DarkBlue;  break;
                case Level.Read:    color = ConsoleColor.DarkMagenta; break;
            }
            return color;
        }

        public static bool Is<T>(string input, ref T number)
        {
            var targetType = typeof(T);
            try
            {
                number = (T)TypeDescriptor.GetConverter(targetType).ConvertFromString(input);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static T ConvertToType<T>(string input)
        {
            try
            {
                return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(input);
            }
            catch
            {
                throw new ArgumentException("Указан неверный тип");
            }
        }
    }
}
