﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core
{
    public enum DataPath
    {
        data, table, text, output, time, cities
    }
}
