﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Project.Core
{
    public class FileWriter : FileWorker
    {
        protected override DataPath Path { get; set; }

        public FileWriter(DataPath? path = DataPath.data)
        {
            if (path == null) return;
            ChangePath((DataPath)path);
        }

        public void WriteFile(string text)
        {
            File.WriteAllText(FullPath, text);
        }
    }
}