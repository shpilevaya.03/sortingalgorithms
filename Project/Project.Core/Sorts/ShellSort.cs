﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Sorts
{
    public class ShellSort : ISort<int[]>, ILogic
    {
        public DataPath? AlgorithmPath => DataPath.data;
        private StringBuilder sb =      new StringBuilder();
        private Logger logger =         new Logger();

        public void Execute(FileReader fileReader, FileWriter fileWriter)
        {
            double timeDelay = 0;
            ConsoleHelper.InputNumberWithText("С какой задержкой выводить на экран последовательные шаги сортировки? (в секундах): ",
                (double num) => num >= 0, out timeDelay);

            int[] elems = Array.ConvertAll(fileReader.ReadFile().Split(new[] { ' ', '\n' },
                StringSplitOptions.RemoveEmptyEntries), x => int.Parse(x));

            Sort(elems);

            logger.PrintLoggerWithDelay(timeDelay);
        }

        public int[] Sort(int[] values)
        {
            int[] elems = (int[])values.Clone();
            Shellsort(elems);

            return values;
        }

        private void Shellsort(int[] values)
        {
            int j;
            int step = values.Length / 2;
            logger.AddLog(Level.Feature, $"Обозначаем количество шагов как длину массива пополам - {step}");

            logger.AddLog(Level.Compare, $"Сравнение количества шагов. {step} > 0");
            while (step > 0)
            {
                for (int i = 0; i < (values.Length - step); i++)
                {
                    j = i;
                    logger.AddLog(Level.Compare, $"Сравнение индекса [{j}] >= 0, которому соответствует элемент {values[j]}");
                    while ((j >= 0) && (values[j] > values[j + step]))
                    {
                        logger.AddLog(Level.Compare, $"Сравнение {values[j]} > {values[j + step]}");
                        logger.AddLog(Level.Swap, $"Меняем местами {values[j]} и {values[j + step]}");
                        int tmp = values[j];
                        values[j] = values[j + step];
                        values[j + step] = tmp;
                        j -= step;
                    }

                    values.ToList().ForEach(x => sb.Append(x.ToString() + " "));
                    logger.AddLog(Level.Array, $"Массив: {sb}");
                    sb.Clear();

                    logger.AddLog(Level.Compare, $"Сравнение [{i}] < [{values.Length - step}], которым соответствуют элементы {values[i]} и {values[values.Length - step]}");
                }
                step = step / 2;

                logger.AddLog(Level.Feature, $"Делим количество шагов ({step}) на 2");

                logger.AddLog(Level.Compare, $"Сравнение количества шагов {step} с 0");
            }

            values.ToList().ForEach(x => sb.Append(x.ToString() + " "));
            logger.AddLog(Level.Array, $"Массив: {sb}");
            sb.Clear();
        }

        public void Dispose()
        {
            logger.ClearLogs();
            sb.Clear();
        }
    }
}
