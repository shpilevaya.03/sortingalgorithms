﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Sorts
{
    public class DirectMergeSort : ILogic
    {
        public DataPath? AlgorithmPath => DataPath.table;
        private Logger logger = new Logger();
        private long _iterations, _segments;
        private const string FileA = "a.csv";
        private const string FileB = "b.csv";
        private int _columnNumber;
        private double time;
        private bool firstTime;
        private string baseType;
        private string FileInput { get; set; }

        public void Execute(FileReader fileReader, FileWriter fileWriter)
        {
            File.Delete(fileWriter.GetPath());
            File.Copy(fileReader.GetPath(), fileWriter.GetPath());
            FileInput = fileWriter.GetPath();
            _iterations = 1;

            using StreamReader sb = new StreamReader(File.OpenRead(FileInput));
            {
                var titles = sb.ReadLine().Split(new[] { '[', ']', ';' },
                    StringSplitOptions.RemoveEmptyEntries);

                ConsoleHelper.InputNumberWithText("Введите индекс атрибута: ", (int num) => num > -1 && num < titles.Length / 2, out _columnNumber);

                baseType = titles[_columnNumber * 2 + 1];
            }
            sb.Close();

            ConsoleHelper.InputNumberWithText("С какой задержкой выводить на экран последовательные шаги сортировки?(в секундах): ", 
                (double num) => num >= 0, out time);

            firstTime = true;
            while (true)
            {
                SplitToFiles();
                if (_segments == 1)
                {
                    break;
                }
                MergePairs();
            }
            Console.WriteLine();

            logger.AddLog(Level.Default, "Файл отсортирован");
            logger.Last().PrintLogWithDelay(time);
        }

        private void SplitToFiles() // разделение на 2 вспом. файла
        {
            _segments = 1;
            using (StreamReader sb = new StreamReader(File.OpenRead(FileInput)))
            using (StreamWriter writerA = new StreamWriter(File.Create(FileA, 65536)))
            using (StreamWriter writerB = new StreamWriter(File.Create(FileB, 65536)))
            {
                if (firstTime) sb.ReadLine();
                firstTime = false;
                long counter = 0;
                bool flag = true; // запись либо в 1-ый, либо во 2-ой файл

                while (!sb.EndOfStream)
                {
                    if (counter == _iterations)
                    {
                        flag = !flag;
                        counter = 0;
                        _segments++;

                        logger.AddLog(Level.Default, $"Записываем серию в файл " + (flag ? "A" : "B"));
                        logger.Last().PrintLogWithDelay(time);
                    }

                    string element = sb.ReadLine();
                    if (element is null) break;
                    if (flag)
                    {
                        writerA.WriteLine(element);
                        logger.AddLog(Level.Record, $"Запись {element} в файл \"{FileA}\"");
                    }
                    else
                    {
                        writerB.WriteLine(element);
                        logger.AddLog(Level.Record, $"Запись {element} в файл \"{FileB}\"");
                    }

                    logger.Last().PrintLogWithDelay(time);

                    counter++;
                }
            }
        }

        private void MergePairs()
        {
            using StreamReader readerA = new StreamReader(File.OpenRead(FileA));
            using StreamReader readerB = new StreamReader(File.OpenRead(FileB));
            using StreamWriter bw = new StreamWriter(File.Create(FileInput));
            {
                long counterA = _iterations, counterB = _iterations;
                string elementA = null, elementB = null, strA = null, strB = null;
                bool pickedA = false, pickedB = false;
                while (!readerA.EndOfStream || !readerB.EndOfStream || pickedA || pickedB)
                {
                    if (counterA == 0 && counterB != 0)
                    {
                        logger.AddLog(Level.Default, $"Серия из \"{FileA}\" закончилась, дописываем {counterB} элементов серии из \"{FileB}\"");
                        logger.Last().PrintLogWithDelay(time);
                    }
                    if (counterB == 0 && counterA != 0)
                    {
                        logger.AddLog(Level.Default, $"Серия из \"{FileB}\" закончилась, дописываем {counterA} элементов серии из \"{FileA}\"");
                        logger.Last().PrintLogWithDelay(time);
                    }

                    if (counterA == 0 && counterB == 0)
                    {
                        counterA = _iterations;
                        counterB = _iterations;
                    }

                    if (!readerA.EndOfStream)
                    {
                        if (counterA > 0 && !pickedA)
                        {
                            strA = readerA.ReadLine();
                            elementA = strA?.Split(';', ',')[_columnNumber];
                            pickedA = true;
                        }
                    }

                    if (!readerB.EndOfStream)
                    {
                        if (counterB > 0 && !pickedB)
                        {
                            strB = readerB.ReadLine();
                            elementB = strB?.Split(';', ',')[_columnNumber];
                            pickedB = true;
                        }
                    }   

                    if (pickedA)
                    {
                        if (pickedB)
                        {
                            logger.AddLog(Level.Compare, "Сравнение " + elementA + " < " + elementB);
                            logger.Last().PrintLogWithDelay(time);

                            if (baseType.Equals("string") && string.Compare(elementA, elementB) < 0 
                                || (elementA.Length < elementB.Length || string.Compare(elementA, elementB) < 0
                                && elementA.Length == elementB.Length) && !baseType.Equals("string"))
                            {
                                bw.WriteLine(strA);

                                logger.AddLog(Level.Record, $"Запись {elementA} в \"{FileInput}\"");

                                counterA--;
                                pickedA = false;
                            }
                            else
                            {
                                bw.WriteLine(strB);

                                logger.AddLog(Level.Record, $"Запись {elementB} в \"{FileInput}\"");

                                counterB--;
                                pickedB = false;
                            }
                        }
                        else
                        {
                            bw.WriteLine(strA);

                            logger.AddLog(Level.Record, $"Запись {elementA} в \"{FileInput}\"");

                            counterA--;
                            pickedA = false;
                        }
                    }
                    else if (pickedB)
                    {
                        bw.WriteLine(strB);

                        logger.AddLog(Level.Record, $"Запись {elementB} в \"{FileInput}\"");

                        counterB--;
                        pickedB = false;
                    }

                    logger.Last().PrintLogWithDelay(time);
                }
                _iterations *= 2;

                logger.AddLog(Level.Default, $"Увеличиваем серию до {_iterations}");
                logger.Last().PrintLogWithDelay(time);
            }
        }

        public void Dispose()
        {
            logger.ClearLogs();
            File.Delete(FileA);
            File.Delete(FileB);
        }
    }
}
