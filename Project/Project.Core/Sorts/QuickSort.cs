﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Sorts
{
    public class QuickSort : ISort<int[]>, ILogic
    {
        public DataPath? AlgorithmPath => DataPath.data;
        private StringBuilder sb =      new StringBuilder();
        private Logger logger =         new Logger();

        public void Execute(FileReader fileReader, FileWriter fileWriter)
        {
            double timeDelay = 0;
            ConsoleHelper.InputNumberWithText("С какой задержкой выводить на экран последовательные шаги сортировки (в секундах)? ",
                (double num) => num >= 0, out timeDelay);

            int[] elems = Array.ConvertAll(fileReader.ReadFile().Split(new[] { ' ', '\n' },
                StringSplitOptions.RemoveEmptyEntries), x => int.Parse(x));

            Sort(elems);

            var output = logger.GetLogs();

            logger.PrintLoggerWithDelay(timeDelay);
        }

        public int[] Sort(int[] values)
        {
            int[] ret = (int[])values.Clone();
            Quicksort(ret, 0, values.Length - 1);

            return ret;
        }

        private int Partition<T>(T[] m, int start, int end) where T : IComparable<T>
        {
            int i = start;
            for (int j = start; j <= end; j++)
            {
                logger.AddLog(Level.Compare, $"Сравнение элементов. {m[j]} <= {m[end]}");
                if (m[j].CompareTo(m[end]) <= 0)
                {
                    logger.AddLog(Level.Swap, $"Меняем местами {m[i]} и {m[j]}");
                    T t = m[i];
                    m[i] = m[j];
                    m[j] = t;
                    i++;
                }
            }
            logger.AddLog(Level.Feature, $"Возвращаем опорный элемент {m[i - 1]} под индексом [{i - 1}]");
            return i - 1;
        }

        private void Quicksort<T>(T[] m, int start, int end) where T : IComparable<T>
        {
            m.ToList().ForEach(x => sb.Append(x.ToString() + " "));
            logger.AddLog(Level.Array, $"Массив: {sb}");
            sb.Clear();

            if (start >= end)
            {
                return;
            }
            logger.AddLog(Level.Compare, $"Сравнение индексов. [{start}] >= [{end}], элементы которых {m[start]} и {m[end]} соотв-но");

            logger.AddLog(Level.Feature, $"Нахождение опорного элемента, относительно которого далее будет сортироваться массив");
            int c = Partition(m, start, end);
            logger.AddLog(Level.Feature, $"Рекурсивная сортировка массива слева относительно опорного элемента от [{start}] до [{c - 1}], эл-ты которых {m[start]} и {m[c == 0 ? 0 : c - 1]} соотв-но");
            Quicksort(m, start, c - 1);
            logger.AddLog(Level.Feature, $"Рекурсивная сортировка массива справа относительно опорного элемента от [{c + 1}] до [{end}], эл-ты которых {m[c + 1 == m.Length ? c : c + 1]} и {m[end == m.Length ? end - 1 : end]} соотв-но");
            Quicksort(m, c + 1, end);
        }

        public void Dispose()
        {
            logger.ClearLogs();
            sb.Clear();
        }
    }
}
