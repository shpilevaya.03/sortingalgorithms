﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Sorts
{
    public class MultipathMergeSort : ILogic
    {
        private string FileInput { get; set; }
        private long _segments;
        private int _columnNumber;
        private const string FileA = "a.csv";
        private const string FileB = "b.csv";
        private const string FileC = "c.csv";
        private double time;
        private bool firstTime;
        private string baseType;
        private string splitChar = "`";

        public DataPath? AlgorithmPath => DataPath.table;
        private Logger logger = new Logger();

        public void Execute(FileReader fileReader, FileWriter fileWriter)
        {
            File.Delete(fileWriter.GetPath());
            File.Copy(fileReader.GetPath(), fileWriter.GetPath());
            FileInput = fileWriter.GetPath();

            //Первая строка -заголовки таблиц и тип[Type]
            using StreamReader sb = new StreamReader(File.OpenRead(FileInput));
            {
                var titles = sb.ReadLine().Split(new[] { '[', ']', ';' },
                    StringSplitOptions.RemoveEmptyEntries);

                ConsoleHelper.InputNumberWithText("Введите индекс атрибута: ", (int num) => num > -1 && num < titles.Length / 2, out _columnNumber);

                baseType = titles[_columnNumber * 2 + 1];
            }
            sb.Close();

            ConsoleHelper.InputNumberWithText("С какой задержкой выводить на экран последовательные шаги сортировки? (в секундах): ",
                (double num) => num >= 0, out time);

            firstTime = true;
            while (true)
            {
                SplitToFiles();
                if (_segments == 1) break;
                MergePairs();
            }

            Console.WriteLine();
            logger.AddLog(Level.Default, "Файл отсортирован");
            logger.Last().PrintLogWithDelay(time);
        }

        private void SplitToFiles()
        {
            _segments = 1;
            logger.AddLog(Level.Split, "Разделение на три файла");
            logger.Last().PrintLogWithDelay(time);

            logger.AddLog(Level.Default, $"Записываем серию в файл A");
            logger.Last().PrintLogWithDelay(time);

            using StreamReader sb = new StreamReader(File.OpenRead(FileInput));
            using StreamWriter writerA = new StreamWriter(File.Create(FileA, 65536));
            using StreamWriter writerB = new StreamWriter(File.Create(FileB, 65536));
            using StreamWriter writerC = new StreamWriter(File.Create(FileC, 65536));
            {
                if (firstTime) sb.ReadLine();
                firstTime = false;
                StreamNumber streamNumber = StreamNumber.StreamA;
                string str1 = null, str2;
                string element1 = null, element2;
                while (true)
                {
                    if (str1 is null)
                    {
                        str1 = sb.ReadLine();
                        element1 = str1?.Split(new[] { ';', ',' })[_columnNumber];
                        writerA.WriteLine(str1);
                    }

                    str2 = sb.ReadLine();
                    if (str2 is null | string.Compare(str2, "", StringComparison.Ordinal) == 0) break;
                    element2 = str2.Split(new[] { ';', ',' })[_columnNumber];

                    if (baseType.Equals("string") && string.Compare(element1, element2) >= 0
                                || (element1.Length > element2.Length || string.Compare(element1, element2) >= 0
                                && element1.Length == element2.Length) && !baseType.Equals("string"))
                    {
                        if (streamNumber == StreamNumber.StreamA)      { writerA.WriteLine(splitChar); streamNumber = StreamNumber.StreamB; logger.AddLog(Level.Default, $"Записываем серию в файл B"); }
                        else if (streamNumber == StreamNumber.StreamB) { writerB.WriteLine(splitChar); streamNumber = StreamNumber.StreamC; logger.AddLog(Level.Default, $"Записываем серию в файл C"); }
                        else if (streamNumber == StreamNumber.StreamC) { writerC.WriteLine(splitChar); streamNumber = StreamNumber.StreamA; logger.AddLog(Level.Default, $"Записываем серию в файл A"); }
                        _segments++;

                        logger.Last().PrintLogWithDelay(time);
                    }

                    if (streamNumber == StreamNumber.StreamA)
                    {
                        writerA.WriteLine(str2);
                        logger.AddLog(Level.Record, $"Запись {element2} в файл \"{FileA}\"");
                    }
                    else if (streamNumber == StreamNumber.StreamB)
                    {
                        writerB.WriteLine(str2);
                        logger.AddLog(Level.Record, $"Запись {element2} в файл \"{FileB}\"");
                    }
                    else if (streamNumber == StreamNumber.StreamC)
                    {
                        writerC.WriteLine(str2);
                        logger.AddLog(Level.Record, $"Запись {element2} в файл \"{FileC}\"");
                    }

                    logger.Last().PrintLogWithDelay(time);

                    str1 = str2;
                    element1 = element2;
                }
            }
        }

        private void MergePairs()
        {
            using StreamReader readerA = new StreamReader(File.OpenRead(FileA));
            using StreamReader readerB = new StreamReader(File.OpenRead(FileB));
            using StreamReader readerC = new StreamReader(File.OpenRead(FileC));
            using StreamWriter bw = new StreamWriter(File.Create(FileInput));
            {
                string elementA = null, elementB = null, strA = null, strB = null, strC = null, elementC = null;
                bool pickedA = false, pickedB = false, pickedC = false, endA = false, endB = false, endC = false, flagA = false, flagB = false, flagC = false;
                while (!endA || !endB || !endC)
                {
                    if (!endA && !pickedA & !flagA)
                    {
                        strA = readerA.ReadLine();
                        if (strA is null | string.Compare(strA, "", StringComparison.Ordinal) == 0) { endA = true; flagA = true; }
                        else
                        {
                            if (strA.Equals(splitChar)) {
                                flagA = true;
                                logger.AddLog(Level.Default, $"Серия из \"{FileA}\" закончилась");
                                logger.Last().PrintLogWithDelay(time);
                            }
                            else { 
                            elementA = strA?.Split(';', ',')[_columnNumber];
                            pickedA = true;
                            }
                        }
                    }

                    if (!endB && !pickedB & !flagB)
                    {
                        strB = readerB.ReadLine();
                        if (strB is null | string.Compare(strB, "", StringComparison.Ordinal) == 0) { endB = true; flagB = true; }
                        else
                        {
                            if (strB.Equals(splitChar)) {
                                flagB = true;
                                logger.AddLog(Level.Default, $"Серия из \"{FileB}\" закончилась");
                                logger.Last().PrintLogWithDelay(time);
                            }
                            else {
                            elementB = strB?.Split(';', ',')[_columnNumber];
                            pickedB = true;
                            }
                        }
                    }

                    if (!endC && !pickedC & !flagC)
                    {
                        strC = readerC.ReadLine();
                        if (strC is null | string.Compare(strC, "", StringComparison.Ordinal) == 0) { endC = true; flagC = true; }
                        else
                        {
                            if (strC.Equals(splitChar)) {
                                flagC = true;
                                logger.AddLog(Level.Default, $"Серия из \"{FileC}\" закончилась");
                                logger.Last().PrintLogWithDelay(time);
                            }
                            else { 
                            elementC = strC?.Split(';', ',')[_columnNumber];
                            pickedC = true;
                            }
                        }
                    }

                    if (flagA && flagB && flagC) { flagA = false; flagB = false; flagC = false; }

                    if (pickedA)
                    {
                        if (pickedB)
                        {
                            if (pickedC)
                            {
                                logger.AddLog(Level.Compare, "Сравнение " + elementA + " и " + elementB);
                                logger.Last().PrintLogWithDelay(time);

                                //compare A and B
                                if (baseType.Equals("string") && string.CompareOrdinal(elementA, elementB) < 0
                                || (elementA.Length < elementB.Length || string.CompareOrdinal(elementA, elementB) < 0
                                && elementA.Length == elementB.Length) && !baseType.Equals("string"))
                                {
                                    //A > B
                                    logger.AddLog(Level.Compare, "Сравнение " + elementA + " и " + elementC);
                                    logger.Last().PrintLogWithDelay(time);

                                    //compare A and C
                                    if (baseType.Equals("string") && string.CompareOrdinal(elementA, elementC) < 0
                                || (elementA.Length < elementC.Length || string.CompareOrdinal(elementA, elementC) < 0
                                && elementA.Length == elementC.Length) && !baseType.Equals("string"))
                                    {
                                        //A > C
                                        logger.AddLog(Level.Record, "Запись " + elementA);
                                        logger.Last().PrintLogWithDelay(time);

                                        bw.WriteLine(strA);

                                        pickedA = false;
                                    }
                                    else
                                    {
                                        //C > A
                                        logger.AddLog(Level.Record, "Запись " + elementC);
                                        logger.Last().PrintLogWithDelay(time);

                                        bw.WriteLine(strC);

                                        pickedC = false;
                                    }
                                }
                                else
                                {
                                    //B > A
                                    logger.AddLog(Level.Compare, "Сравнение " + elementB + " и " + elementC);
                                    logger.Last().PrintLogWithDelay(time);

                                    //compare B and C
                                    if (baseType.Equals("string") && string.CompareOrdinal(elementB, elementC) < 0
                                || (elementB.Length < elementC.Length || string.CompareOrdinal(elementB, elementC) < 0
                                && elementB.Length == elementC.Length) && !baseType.Equals("string"))
                                    {
                                        //B > C
                                        logger.AddLog(Level.Record, "Запись " + elementB);
                                        logger.Last().PrintLogWithDelay(time);

                                        bw.WriteLine(strB);

                                        pickedB = false;
                                    }
                                    else
                                    {
                                        //C > B
                                        logger.AddLog(Level.Record, "Запись " + elementC);
                                        logger.Last().PrintLogWithDelay(time);

                                        bw.WriteLine(strC);

                                        pickedC = false;
                                    }
                                }
                            }

                            else
                            {
                                //not picked C
                                //compare A and B
                                logger.AddLog(Level.Compare, "Сравнение " + elementA + " и " + elementB);
                                logger.Last().PrintLogWithDelay(time);

                                if (baseType.Equals("string") && string.CompareOrdinal(elementA, elementB) < 0
                                || (elementA.Length < elementB.Length || string.CompareOrdinal(elementA, elementB) < 0
                                && elementA.Length == elementB.Length) && !baseType.Equals("string"))
                                {
                                    //A > B
                                    bw.WriteLine(strA);

                                    logger.AddLog(Level.Record, "Запись " + elementA);
                                    logger.Last().PrintLogWithDelay(time);

                                    pickedA = false;
                                }
                                else
                                {
                                    //B > A
                                    bw.WriteLine(strB);

                                    logger.AddLog(Level.Record, "Запись " + elementB);
                                    logger.Last().PrintLogWithDelay(time);

                                    pickedB = false;
                                }
                            }
                        }
                        else if (pickedC)
                        {
                            //not picked B
                            logger.AddLog(Level.Compare, "Сравнение " + elementA + " и " + elementC);
                            logger.Last().PrintLogWithDelay(time);

                            //compare A and C
                            if (baseType.Equals("string") && string.CompareOrdinal(elementA, elementC) < 0
                                || (elementA.Length < elementC.Length || string.CompareOrdinal(elementA, elementC) < 0
                                && elementA.Length == elementC.Length) && !baseType.Equals("string"))
                            {
                                //A > C
                                bw.WriteLine(strA);

                                logger.AddLog(Level.Record, "Запись " + elementA);
                                logger.Last().PrintLogWithDelay(time);

                                pickedA = false;
                            }
                            else
                            {
                                //C > A
                                bw.WriteLine(strC);

                                logger.AddLog(Level.Record, "Запись " + elementC);
                                logger.Last().PrintLogWithDelay(time);

                                pickedC = false;
                            }
                        }
                        else
                        {
                            //picked only A
                            bw.WriteLine(strA);

                            logger.AddLog(Level.Record, "Запись " + elementA);
                            logger.Last().PrintLogWithDelay(time);

                            pickedA = false;
                        }
                    }

                    else if (pickedB && pickedC)
                    {
                        //picked B and C without A
                        logger.AddLog(Level.Compare, "Сравнение " + elementB + " и " + elementC);
                        logger.Last().PrintLogWithDelay(time);

                        //compare B and C
                        if (baseType.Equals("string") && string.CompareOrdinal(elementB, elementC) < 0
                                || (elementB.Length < elementC.Length || string.CompareOrdinal(elementB, elementC) < 0
                                && elementB.Length == elementC.Length) && !baseType.Equals("string"))
                        {
                            //B > C
                            logger.AddLog(Level.Record, "Запись " + elementB);
                            logger.Last().PrintLogWithDelay(time);

                            bw.WriteLine(strB);

                            pickedB = false;
                        }
                        else
                        {
                            //C > B
                            logger.AddLog(Level.Record, "Запись " + elementC);
                            logger.Last().PrintLogWithDelay(time);

                            bw.WriteLine(strC);

                            pickedC = false;
                        }
                    }
                    else if (pickedC)
                    {
                        //picked only C
                        logger.AddLog(Level.Record, "Запись " + elementC);
                        logger.Last().PrintLogWithDelay(time);

                        bw.WriteLine(strC);

                        pickedC = false;
                    }
                    else if (pickedB)
                    {
                        //picked only B
                        logger.AddLog(Level.Record, "Запись " + elementB);
                        logger.Last().PrintLogWithDelay(time);

                        bw.WriteLine(strB);

                        pickedB = false;
                    }
                }
            }
        }

        public void Dispose()
        {
            logger.ClearLogs();
            File.Delete(FileA);
            File.Delete(FileB);
            File.Delete(FileC);
        }
    }
}
