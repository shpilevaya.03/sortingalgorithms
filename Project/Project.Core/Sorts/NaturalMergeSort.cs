﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Sorts
{
    public class NaturalMergeSort : ILogic
    {
        private string FileInput { get; set; }
        private const string FileA = "a.csv";
        private const string FileB = "b.csv";
        private long _iterations, _segments;
        private int _columnNumber;
        private double time;
        private bool firstTime;
        private string baseType;
        private string splitChar = "`";
        public DataPath? AlgorithmPath => DataPath.table;
        private Logger logger = new Logger();

        public void Execute(FileReader fileReader, FileWriter fileWriter)
        {
            File.Delete(fileWriter.GetPath());
            File.Copy(fileReader.GetPath(), fileWriter.GetPath());
            FileInput = fileWriter.GetPath();

            //Первая строка -заголовки таблиц и тип[Type]
            using StreamReader sb = new StreamReader(File.OpenRead(FileInput));
            {
                var titles = sb.ReadLine().Split(new[] { '[', ']', ';' },
                    StringSplitOptions.RemoveEmptyEntries);

                ConsoleHelper.InputNumberWithText("Введите индекс атрибута: ", (int num) => num > -1 && num < titles.Length / 2, out _columnNumber);

                baseType = titles[_columnNumber * 2 + 1];
            }
            sb.Close();

            ConsoleHelper.InputNumberWithText("С какой задержкой выводить на экран последовательные шаги сортировки? (в секундах): ",
                (double num) => num >= 0, out time);

            firstTime = true;
            while (true)
            {
                SplitToFiles();
                if (_segments == 1) break;
                MergePairs();
            }

            Console.WriteLine();
            logger.AddLog(Level.Default, "Файл отсортирован");
            logger.Last().PrintLogWithDelay(time);
        }

        private void SplitToFiles()
        {
            _segments = 1;

            logger.AddLog(Level.Split, "Разделение на два файла");
            logger.Last().PrintLogWithDelay(time);

            logger.AddLog(Level.Default, $"Записываем серию в файл A");
            logger.Last().PrintLogWithDelay(time);

            using StreamReader sb = new StreamReader(File.OpenRead(FileInput));
            using StreamWriter writerA = new StreamWriter(File.Create(FileA));
            using StreamWriter writerB = new StreamWriter(File.Create(FileB));
            {
                if(firstTime) sb.ReadLine();
                firstTime = false;
                bool flag = true;
                string str1 = null, str2;
                string element1 = null, element2;
                while (true)
                {
                    if (str1 is null)
                    {
                        str1 = sb.ReadLine();
                        element1 = str1?.Split(new[] { ';', ',' })[_columnNumber];
                        writerA.WriteLine(str1);
                    }

                    str2 = sb.ReadLine();
                    if (str2 is null | string.Compare(str2, "", StringComparison.Ordinal) == 0) break;
                    element2 = str2.Split(new[] { ';', ',' })[_columnNumber];

                    //compare elem1 and elem2
                    if (baseType.Equals("string") && string.Compare(element1, element2) >= 0
                                || (element1.Length > element2.Length || string.Compare(element1, element2) >= 0
                                && element1.Length == element2.Length) && !baseType.Equals("string"))
                    {
                        //elem1 >= elem2
                        if (flag) writerA.WriteLine(splitChar); else writerB.WriteLine(splitChar);
                        flag = !flag;
                        _segments++;

                        logger.AddLog(Level.Default, $"Записываем серию в файл " + (flag ? "A" : "B"));
                        logger.Last().PrintLogWithDelay(time);
                    }

                    if (flag)
                    {
                        writerA.WriteLine(str2);
                        logger.AddLog(Level.Record, $"Запись {element2} в файл \"{FileA}\"");
                    }
                    else
                    {
                        writerB.WriteLine(str2);
                        logger.AddLog(Level.Record, $"Запись {element2} в файл \"{FileB}\"");
                    }

                    logger.Last().PrintLogWithDelay(time);

                    str1 = str2;
                    element1 = element2;
                }
            }
        }

        private void MergePairs()
        {
            using StreamReader readerA = new StreamReader(File.OpenRead(FileA));
            using StreamReader readerB = new StreamReader(File.OpenRead(FileB));
            using StreamWriter bw = new StreamWriter(File.Create(FileInput));
            {
                string elementA = null, elementB = null, strA = null, strB = null;
                bool pickedA = false, pickedB = false, endA = false, endB = false, flagA = false, flagB = false;
                while (!endA || !endB)
                {
                    if (!endA & !pickedA & !flagA)
                    {
                        strA = readerA.ReadLine();
                        if (strA is null | string.Compare(strA, "", StringComparison.Ordinal) == 0) { endA = true; flagA = true; }
                        else
                        {
                            if (strA.Equals(splitChar))
                            {
                                flagA = true;
                                if (!flagB)
                                {
                                    logger.AddLog(Level.Default, $"Серия из \"{FileA}\" закончилась, дописываем оставшиеся элементы серии из \"{FileB}\"");
                                    logger.Last().PrintLogWithDelay(time);
                                }
                            }
                            else
                            {
                                elementA = strA.Split(';', ',')[_columnNumber];
                                pickedA = true;
                            }
                        }
                    }

                    if (!endB & !pickedB & !flagB)
                    {
                        strB = readerB.ReadLine();
                        if (strB is null | string.Compare(strB, "", StringComparison.Ordinal) == 0) { endB = true; flagB = true; }
                        else
                        {
                            if (strB.Equals(splitChar))
                            {
                                flagB = true;
                                if (!flagA) {
                                    logger.AddLog(Level.Default, $"Серия из \"{FileB}\" закончилась, дописываем оставшиеся элементы серии из \"{FileA}\"");
                                    logger.Last().PrintLogWithDelay(time);
                                }
                            }
                            else
                            {
                                elementB = strB.Split(';', ',')[_columnNumber];
                                pickedB = true;
                            }
                        }
                    }

                    if(flagA && flagB) { flagA = false; flagB = false; }

                    if (pickedA)
                    {
                        if (pickedB)
                        {
                            logger.AddLog(Level.Compare, "Сравнение " + elementA + " и " + elementB);
                            logger.Last().PrintLogWithDelay(time);

                            if (baseType.Equals("string") && string.CompareOrdinal(elementA, elementB) < 0
                                || (elementA.Length < elementB.Length || string.CompareOrdinal(elementA, elementB) < 0
                                && elementA.Length == elementB.Length) && !baseType.Equals("string"))
                            {
                                bw.WriteLine(strA); 

                                logger.AddLog(Level.Record, "Запись " + elementA);
                                logger.Last().PrintLogWithDelay(time);

                                pickedA = false;
                            }
                            else
                            {
                                bw.WriteLine(strB);

                                logger.AddLog(Level.Record, "Запись " + elementB);
                                logger.Last().PrintLogWithDelay(time);

                                pickedB = false;
                            }
                        }
                        else
                        {
                            bw.WriteLine(strA);

                            logger.AddLog(Level.Record, "Запись " + elementA);
                            logger.Last().PrintLogWithDelay(time);

                            pickedA = false;
                        }
                    }
                    else if(pickedB)
                    {
                        bw.WriteLine(strB);

                        logger.AddLog(Level.Record, "Запись " + elementB);
                        logger.Last().PrintLogWithDelay(time);

                        pickedB = false;
                    }
                }
            }
        }

        public void Dispose()
        {
            logger.ClearLogs();
            File.Delete(FileA);
            File.Delete(FileB);
        }
    }
}