﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Sorts
{
    public class TextQuickSort : ISort<string[]>, ILogic
    {
        public DataPath? AlgorithmPath => DataPath.text;
        private int[] wordTracker;
        private int[,] letterTracker;
        private List<string> result = new List<string>();

        public void Execute(FileReader fileReader, FileWriter fileWriter)
        {
            string[] values = fileReader.ReadWords();

            //AlgorithmWorker.RepeatMeasure(values.ToList(), Sort, 100, 250);

            string[] sortedArray = Sort(values);
            ConsoleHelper.PrintHighlightedText("Слово и количество раз, которое оно встретилось");
            for (int i = 0; i < sortedArray.Length; i++)
            {
                var value = sortedArray[i].ToLower();
                int count = values.Count(x => x.ToLower() == value);
                Console.WriteLine($"\"{value}\"{new string(' ', 20 - value.Length)} — {count}");
                i += count;
            }
        }

        public string[] Sort(string[] values)
        {
            string[] words = new string[values.Length];
            letterTracker = new int[15, 26];
            wordTracker = new int[words.Length];
            Dispose();

            values.CopyTo(words, 0);
            SetLevels(words, 0);

            Recursive(words, 1);

            return result.ToArray();
        }

        private void Recursive(string[] words, int depth)
        {
            for (int j = 0; j < letterTracker.GetLength(1); j++)
            {
                int index = letterTracker[depth - 1, j];
                if (index != 0 && wordTracker[index - 1] == 0)
                {
                    result.Add(words[index - 1]);
                    index = 0;
                }
                letterTracker[depth - 1, j] = 0;

                int copy = index;
                while (index != 0)
                {
                    if (words[index - 1].Length <= depth) { result.Add(words[index - 1]);
                        int next = wordTracker[index - 1];
                        index = next;
                    }
                    else
                    {
                        int order = Convert.ToByte(words[index - 1].ToUpper()[depth]) - 65;
                        int next = wordTracker[index - 1];
                        wordTracker[index - 1] = letterTracker[depth, order];
                        letterTracker[depth, order] = index;
                        index = next;
                    }
                }

                if(copy != 0 && depth < letterTracker.GetLength(0) - 1) Recursive(words, depth + 1);
            }
        }

        private void SetLevels(string[] values, int direction)
        {
            for (int i = 1; i < values.Length + 1; i++)
            {
                int order = Convert.ToByte(values[i - 1].ToUpper()[direction]) - 65;
                wordTracker[i - 1] = letterTracker[direction, order];
                letterTracker[direction, order] = i;
            }
        }

        public void Dispose()
        {
            result.Clear();
        }
    }
}