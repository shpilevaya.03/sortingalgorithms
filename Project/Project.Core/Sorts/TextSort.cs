﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Sorts
{
    public class TextSort : ISort<string[]>, ILogic
    {
        public DataPath? AlgorithmPath => DataPath.text;

        public void Execute(FileReader fileReader, FileWriter fileWriter)
        {
            string[] values = fileReader.ReadWords();

            //AlgorithmWorker.RepeatMeasure(values.ToList(), Sort, 100, 250);

            string[] sortedArray = Sort(values);
            ConsoleHelper.PrintHighlightedText("Слово и количество раз, которое оно встретилось");
            for(int i = 0; i < sortedArray.Length; i++)
            {
                var value = sortedArray[i].ToLower();
                int count = values.Count(x => x.ToLower() == value);
                Console.WriteLine($"\"{value}\"{new string(' ', 20-value.Length)} — {count}");
                i+=count;
            }
        }

        public string[] Sort(string[] values)
        {
            for (int i = 0; i < values.Length - 1; i++)
            {
                for (int j = i + 1; j < values.Length; j++)
                {
                    if (values[i].ToLower().CompareTo(values[j].ToLower()) > 0)
                    {
                        string swap = values[i];
                        values[i] = values[j];
                        values[j] = swap;
                    }
                }
            }
            return values;
        }

        public void Dispose()
        {

        }
    }
}
