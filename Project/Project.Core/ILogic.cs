﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core;

namespace Project.Core
{
    public interface ILogic
    {
        DataPath? AlgorithmPath { get; }
        public void Execute(FileReader fileReader, FileWriter fileWriter);
        public void Dispose();
    }
}
