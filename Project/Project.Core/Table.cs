﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core
{
    public class Table<T> where T : IComparable<T>
    {
        public class Column
        {
            private string Name { get; set; }
            private List<T> Values { get; set; }
            private int Index { get; set; }

            public Column(string name)
            {
                Name = name;
                Values = new List<T>();
            }

            public Column(string name, List<T> values)
            {
                Name = name;
                Values = values;
            }

            public void AddValue(T value)
            {
                Values.Add(value);
            }
        }
        
        private List<Column> Columns { get; set; }

        public Table()
        {
            Columns = new List<Column>();
        }

        public List<Column> GetColumns()
        {
            return Columns.ToList();
        }

        public void AddColumn(string name)
        {
            Columns.Add(new Column(name));
        }

        public void AddColumn(string name, List<T> values)
        {
            Columns.Add(new Column(name, values));
        }

        public void AddColumnValue(int index, T value)
        {
            Columns[index].AddValue(value);
        }
    }
}
