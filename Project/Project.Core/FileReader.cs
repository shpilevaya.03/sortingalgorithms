﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Project.Core
{
    public class FileReader : FileWorker
    {
        protected override DataPath Path { get; set; }

        public FileReader(DataPath? path = DataPath.data)
        {
            if (path == null) return;
            ChangePath((DataPath)path);
        }

        public string ReadFile()
        {
            return File.ReadAllText(FullPath);
        }

        public string[] ReadWords()
        {
            char[] separators = new[] { ' ', '\n', '\r', '\t', ';', '.', '!', '?', ',', ':', '-', '+', '=', '*', '\\', '/', '—', '<', '>', '\"', '\'', '[', ']', '(', ')', '{', '}' };

            var split = ReadFile().Split(separators, StringSplitOptions.RemoveEmptyEntries);
            return split;
        }
    }
}
