﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project.Core
{
    public enum Level {
        Swap, Compare, Array, Record, Split, Feature, Default, Read
    }

    public class Logger
    {
        public class Log
        {
            private Level _level { get; set; }
            private string _message { get; set; }

            public Log(Level level, string message)
            {
                _level = level;
                _message = message;
            }

            public string ToString()
            {
                return $"[{Enum.GetName(typeof(Level), _level).ToUpper()}]{new string(' ', 7 - Enum.GetName(typeof(Level), _level).Count())} - {_message}";
            }

            public void PrintLog()
            {
                Console.Write("[");
                Console.Write(Enum.GetName(typeof(Level), _level).ToUpper(), Console.ForegroundColor = ConsoleHelper.GetLevelColor(_level));
                Console.ResetColor();
                Console.WriteLine($"]{new string(' ', 7 - Enum.GetName(typeof(Level), _level).Count())} - {_message}");
            }

            public void PrintLogWithDelay(double timeDelay)
            {
                PrintLog();
                Thread.Sleep((int)(timeDelay * 1000));
            }
        }

        private List<Log> logs = new List<Log>();

        public void AddLog(Level level, string message) => logs.Add(new Log(level, message));

        public List<Log> GetLogs()
        { 
            return logs.ToList();
        }

        public void ClearLogs() => logs.Clear();

        public void PrintLoggerWithDelay(double timeDelay)
        {
            for (int i = 0; i < logs.Count; i++)
            {
                logs[i].PrintLog();
                Thread.Sleep((int)(timeDelay * 1000));
            }
        }

        public Log Last()
        {
            return logs[logs.Count - 1];
        }
    }
}
