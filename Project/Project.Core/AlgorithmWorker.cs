﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Diagnostics;
using Project.Core;

namespace Project
{
    public static class AlgorithmWorker
    {
        public static FileWriter fileWriter =   new FileWriter(DataPath.time);
        public static StringBuilder text =      new StringBuilder();

        public static void MeasureExecutionTime<T>(T arr, Func<T, T> func)
        {
            long freq = Stopwatch.Frequency; //частота таймера
            Stopwatch stopwatch = new Stopwatch();
            var results = new List<double>();

            for (int i = 0; i < 5; i++)
            {
                stopwatch.Reset();
                stopwatch.Start();
                func(arr);
                stopwatch.Stop();
                double sec = (double)stopwatch.ElapsedTicks / freq; //переводим такты в секунды
                Console.WriteLine($"Время выполнения в тактах \t{stopwatch.ElapsedTicks}\r\nВремя в секундах \t\t{sec}\r\n");
                results.Add(sec);
            }

            results.Remove(results.Max());

            Console.WriteLine($"Среднее время выполнения алгоритма: {results.Sum() / results.Count()} с\r\n" +
                new string('-', 20) + "\r\n");

            text.Append(results.Sum() / results.Count());
        }

        public static void RepeatMeasure(List<string> info, Func<string[], string[]> func, int count, int step)
        {
            text.Clear();

            for (int i = 0; i < step; i++)
            {
                string[] arr = info.GetRange(0, count * (i + 1)).ToArray();
                Console.WriteLine($"Количество элементов: {arr.Length}");
                text.Append(arr.Length + " ");
                MeasureExecutionTime(arr, func);
                text.AppendLine();
                GC.Collect();
            }

            fileWriter.WriteFile(text.ToString());
        }
    }
}
