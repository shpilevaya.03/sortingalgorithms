﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Project.Core
{
    public static class GenerationData
    {
        /// <summary>Генерирует список рандомных чисел, количество которых равно count</summary>
        public static List<int> GenerateData(int count)
        {
            int[] data = new int[count];
            Random rand = new Random();

            for (int i = 0; i < count; i++)
            {
                data[i] = rand.Next(100);
            }

            return data.ToList();
        }
    }
}